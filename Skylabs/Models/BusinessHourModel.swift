//
//  BusinessHourModel.swift
//  Skylabs
//
//  Created by Johnny De Marco on 28/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

class BusinessHourModel: NSObject {
    var day : String?
    var hours : Array<String>?
    
    init(dictionary: Dictionary<String, Any>)
    {
        super.init()
        if let day = dictionary["day"] as? String
        {
            self.day = day
        }
        if let schedules = dictionary["schedules"] as? [String]
        {
            hours = Array<String>()
            for schedule in schedules
            {
                self.hours?.append(schedule)
            }
        }
    }
}
