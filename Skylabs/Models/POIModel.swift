//
//  POIModel.swift
//  Skylabs
//
//  Created by Johnny De Marco on 28/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

class POIModel: NSObject {
    
    var address : String?
    var descriptionPOI : String?
    var imagePath : String?
    var lat : String?
    var lng : String?
    var name : String?
    var image : UIImage?
    var businessHours : Array<BusinessHourModel>?
    
    override init(){
        super.init()
    }
    init(dictionary: Dictionary<String, Any>)
    {
        super.init()
        if let address = dictionary["address"] as? String
        {
            self.address = address
        }
        if let description = dictionary["description"] as? String
        {
            self.descriptionPOI = description
        }
        if let imagePath = dictionary["imagePath"] as? String
        {
            self.imagePath = imagePath
        }
        if let lat = dictionary["lat"] as? String
        {
            self.lat = lat
        }
        if let lng = dictionary["lng"] as? String
        {
            self.lng = lng
        }
        if let name = dictionary["name"] as? String
        {
            self.name = name
        }
        if let businessHours = dictionary["businessHours"] as? [Dictionary<String, Any>]
        {
            self.businessHours = Array<BusinessHourModel>()
            for business in businessHours
            {
                let bus = BusinessHourModel(dictionary: business)
                self.businessHours?.append(bus)
            }
        }

        
    }
}
