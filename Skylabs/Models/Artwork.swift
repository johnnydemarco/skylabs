//
//  Artwork.swift
//  Skylabs
//
//  Created by Johnny De Marco on 30/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import Foundation
import MapKit

class Artwork: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    let poi: POIModel
    
    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D, poi: POIModel) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        self.poi = poi
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
