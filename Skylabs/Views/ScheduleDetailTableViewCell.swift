//
//  ScheduleDetailTableViewCell.swift
//  Skylabs
//
//  Created by Johnny De Marco on 29/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

class ScheduleDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var mondayTimeLabel: UILabel!
    @IBOutlet weak var tuesdayTimeLabel: UILabel!
    @IBOutlet weak var wednesdayTimeLabel: UILabel!
    @IBOutlet weak var thursdayTimeLabel: UILabel!
    @IBOutlet weak var fridayTimeLabel: UILabel!
    @IBOutlet weak var saturdayTimeLabel: UILabel!
    @IBOutlet weak var sundayTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
