//
//  DetailTableViewCell.swift
//  Skylabs
//
//  Created by Johnny De Marco on 29/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imageDetailView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageDetailView.layer.cornerRadius = 20.0
        self.imageDetailView.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
