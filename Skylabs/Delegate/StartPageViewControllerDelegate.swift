//
//  StartPageViewControllerDelegate.swift
//  Skylabs
//
//  Created by Johnny De Marco on 30/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

protocol StartPageDelegate {
    func loadingViewShow(bool: Bool)
    func goToDetail(poi: POIModel)
    func listFiltered(list: [POIModel])
    
}

class StartPageViewControllerDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var delegate : StartPageDelegate?
    var list = [POIModel]()
    var listFiltered = [POIModel]()

    let manager = ActionManager()
    
    //MARK: PERFORM
    func goToDetail(withPOI poi:POIModel)
    {
        if(poi.image?.imageAsset == nil && poi.imagePath!.characters.count > 0 )
        {
            if(delegate != nil)
            {
                delegate?.loadingViewShow(bool: true)
            }
            manager.takeImage(path: poi.imagePath!, responseBlock: { (image) in
                poi.image = image
                if(self.delegate != nil)
                {
                    self.delegate?.loadingViewShow(bool: false)
                    self.delegate?.goToDetail(poi: poi)
                }
            })
        }
        else
        {
            if(delegate != nil)
            {
                delegate?.goToDetail(poi: poi)
            }
        }
    }
    
    //MARK: TABLEVIEWDELEGATE AND TABLEVIEWDATASOURCE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.count > 0 ? self.list.count : 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.allowsSelection = false
        goToDetail(withPOI: self.list[indexPath.row])
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "List", for: indexPath) as! ListTableViewCell
        let poi = list[indexPath.row]
        cell.nameLabel.text = poi.name!
        cell.descriptionLabel.text = poi.address!
        
        return cell
    }
    
    //PRIVATE METHODS
    func needFilteredList()
    {
        let today = takeDay()
        for poi in list
        {
            takeArrayTimeForDay(poi: poi, day: today)
        }
        if(delegate != nil)
        {
            delegate?.listFiltered(list: listFiltered)
        }
    }
    
    //MARK: Filtering List
    func takeDay() -> String
    {
        let weekday = Calendar.current.component(.weekday, from: Date()) - 1
        let f = DateFormatter()
        let string = f.weekdaySymbols[weekday]
        let index = string.characters.index(string.startIndex, offsetBy: 2)
        return String(string[...index]).lowercased()
    }
    
    func takeArrayTimeForDay(poi: POIModel, day: String)
    {
        guard poi.businessHours != nil else
        {
            return
        }
        for p in poi.businessHours!
        {
            if(p.day == day)
            {
                if(filteredList(time: p.hours))
                {
                    listFiltered.append(poi)
                }
            }
        }
    }
    
    func filteredList(time: Array<String>?) -> Bool
    {
        guard time != nil else{
            return false
        }
        for timeString in time!
        {
            let fullNameArr = timeString.components(separatedBy: " - ")
            let start: String = fullNameArr[0]
            let finish: String = fullNameArr.count > 1 ? fullNameArr[1] : "0"
            
            let startArray = start.components(separatedBy: ":")
            let start_hour =  Int(startArray[0])!
            let start_minute = Int(startArray[1])!
            let finishArray = finish.components(separatedBy: ":")
            let finish_hour =  Int(finishArray[0])!
            let finish_minute = Int(finishArray[1])!
            
            let calendar = Calendar.current
            let now = Date()
            let starting = calendar.date(
                bySettingHour: start_hour,
                minute: start_minute,
                second: 0,
                of: now)!
            
            let finishing = calendar.date(
                bySettingHour: finish_hour,
                minute: finish_minute,
                second: 0,
                of: now)!
            
            if now >= starting &&
                now <= finishing
            {
                return true
            }
        }
        
        return false
    }
    
}
