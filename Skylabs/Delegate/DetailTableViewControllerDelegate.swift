//
//  DetailTableViewControllerDelegate.swift
//  Skylabs
//
//  Created by Johnny De Marco on 30/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

class DetailTableViewControllerDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var poi = POIModel()
    
    init(poi: POIModel)
    {
        self.poi = poi
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Detail", for: indexPath) as! DetailTableViewCell
            cell.nameLabel.text = poi.name
            cell.addressLabel.text = poi.address
            if(poi.image != nil)
            {
                cell.imageDetailView?.image = poi.image
            }
            else
            {
                cell.imageDetailView?.image = UIImage(named: "no_image")
            }
            return cell
        }
        else if(indexPath.row == 1)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionDetail", for: indexPath) as! DescriptionDetailTableViewCell
            cell.descriptionLabel.text = poi.descriptionPOI
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleDetail", for: indexPath) as! ScheduleDetailTableViewCell
            guard poi.businessHours != nil else
            {
                return cell
            }
            for p in poi.businessHours!
            {
                if(p.day == "mon")
                {
                    cell.mondayTimeLabel.text = (p.hours != nil) ? fillScheduleTime(hours: p.hours!) : "Chiuso"
                }
                if(p.day == "tue")
                {
                    cell.tuesdayTimeLabel.text = (p.hours != nil) ? fillScheduleTime(hours: p.hours!) : "Chiuso"
                }
                if(p.day! == "wed")
                {
                    cell.wednesdayTimeLabel.text = (p.hours != nil) ? fillScheduleTime(hours: p.hours!) : "Chiuso"
                }
                if(p.day! == "thu")
                {
                    cell.thursdayTimeLabel.text = (p.hours != nil) ? fillScheduleTime(hours: p.hours!) : "Chiuso"
                }
                if(p.day! == "fri")
                {
                    cell.fridayTimeLabel.text = (p.hours != nil) ? fillScheduleTime(hours: p.hours!) : "Chiuso"
                }
                if(p.day! == "sat")
                {
                    cell.saturdayTimeLabel.text = (p.hours != nil) ? fillScheduleTime(hours: p.hours!) : "Chiuso"
                }
                if(p.day! == "sun")
                {
                    cell.sundayTimeLabel.text = (p.hours != nil) ? fillScheduleTime(hours: p.hours!) : "Chiuso"
                }
                
            }
            return cell
        }
    }
    
    func fillScheduleTime(hours: Array<String>) -> String
    {
        var scheduleString : String = ""
        guard hours.count > 0 else {
            scheduleString = "Chiuso"
            return scheduleString
        }
        if (hours[0].characters.count > 0)
        {
            scheduleString.append(hours[0])
        }
        if(hours[1].characters.count > 0)
        {
            scheduleString.append("/\(hours[1])")
        }
        return scheduleString
    }
    
}
