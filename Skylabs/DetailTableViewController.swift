//
//  DetailTableViewController.swift
//  Skylabs
//
//  Created by Johnny De Marco on 29/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {

    @IBOutlet var table: UITableView!
    var poi = POIModel()
    var delegate : DetailTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
    }
    //MARK: PRIVATE METHODS
    func registerNib(){
        table.register(UINib.init(nibName: "DetailTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Detail")
        table.register(UINib.init(nibName: "ScheduleDetailTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ScheduleDetail")
        table.register(UINib.init(nibName: "DescriptionDetailTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "DescriptionDetail")
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 220
        delegate = DetailTableViewControllerDelegate(poi: poi)
        table.delegate = delegate
        table.dataSource = delegate
    }
}
