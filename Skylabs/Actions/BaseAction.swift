//
//  BaseAction.swift
//  Skylabs
//
//  Created by Johnny De Marco on 28/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit
import Alamofire

class BaseAction: NSObject {
    var url : String = ""
    
    func callWithName(actionName : String) -> Void {
        url = actionName
    }
    
    func executeAction(params: Parameters, success successBlock: @escaping ((Alamofire.DataResponse<Any>) -> Void), failure failureBlock: @escaping ((Alamofire.DataResponse<Any>) -> Void)) {
        
        let parameters : Parameters = params
        let urlString : String = "http://www.skylabs.it/mobileassignment/assignment1.json" + url
        let webView : UIWebView = UIWebView.init(frame: CGRect.zero)
        let defaultUA : String = webView.stringByEvaluatingJavaScript(from: "navigator.userAgent")!
        let parametersLength: String = String.init(format: "%d",parameters.count)
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
            "Accept-Language": "it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4",
            "User-Agent": defaultUA + " {SUPER_APP}{APPAPPLE}",
            "Content-Length": parametersLength
        ]
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            switch response.result{
            case .success(let JSON):
                let json = JSON as! Dictionary<String, AnyObject>
                print(JSON)
                if let _ = json["res"] as? String
                {
                    successBlock(response)
                }
                else
                {
                    failureBlock(response)
                }
            default:
                failureBlock(response)
                print("errore chiamata")
            }
        }
    }
}
