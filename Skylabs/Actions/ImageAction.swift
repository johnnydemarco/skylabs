//
//  ImageAction.swift
//  Skylabs
//
//  Created by Johnny De Marco on 30/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit
import Alamofire

class ImageAction: NSObject {

    func downloadImage(path: String, responseBlock: @escaping ((UIImage) -> Void))
    {
        let remoteImageURL = URL(string: path)!
        Alamofire.request(remoteImageURL).responseData { (response) in
            if response.error == nil {
                if let data = response.data {
                    if(UIImage(data: data) != nil)
                    {
                        responseBlock(UIImage(data: data)!)
                    }
                    else
                    {
                        responseBlock(UIImage(named: "no_image")!)
                    }
                }
            }
            else{
                responseBlock(UIImage(named: "no_image")!)
                //Errore download immagine
            }
        }
    }
}
