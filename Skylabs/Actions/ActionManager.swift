//
//  ActionManager.swift
//  Skylabs
//
//  Created by Johnny De Marco on 28/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

class ActionManager
{

    func takeListAction(responseBlock : @escaping ((Array<POIModel>) -> Void) )
    {
        let listAction = ListAction()
        listAction.takeList { (list) in
            responseBlock(list)
        }
    }
    func takeImage(path: String ,responseBlock: @escaping ((UIImage) -> Void))
    {
        let imageAction = ImageAction()
        imageAction.downloadImage(path: path) { (image) in
            responseBlock(image)
        }
    }
}
