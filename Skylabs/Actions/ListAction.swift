//
//  ListAction.swift
//  Skylabs
//
//  Created by Johnny De Marco on 28/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit

class ListAction: BaseAction {
    
    var list : Array<POIModel>?
    
    func takeList(responseBlock : @escaping ((Array<POIModel>) -> Void) )
    {
        super.executeAction(params: [:], success: { (response) in
            switch response.result{
            case .success(let JSON):
                guard let json = JSON as? Dictionary<String, AnyObject> else
                {
                    return
                }
                if let dataList = json["data"] as? [Dictionary<String, AnyObject>]
                {
                    self.list = Array<POIModel>()
                    for data in dataList
                    {
                        let poi = POIModel(dictionary: data)
                        self.list?.append(poi)
                    }
                }
                responseBlock(self.list!)
            default:
                responseBlock(Array<POIModel>())
            }
        }) { (error) in
            responseBlock(Array<POIModel>())
            print(error.description)
        }
    }

}
