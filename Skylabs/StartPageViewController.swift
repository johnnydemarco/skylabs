//
//  StartPageViewController.swift
//  Skylabs
//
//  Created by Johnny De Marco on 28/09/17.
//  Copyright © 2017 johnny. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

class StartPageViewController: ViewController , StartPageDelegate {
    
    @IBOutlet weak var filterSwitch: UISwitch!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var kindOfVisual: UISegmentedControl!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    let manager = ActionManager()
    let delegate = StartPageViewControllerDelegate()
    
    var list = [POIModel]()
    var artworks : [Artwork] = []
    var artworksFiltered : [Artwork] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = delegate
        self.tableView.dataSource = delegate
        self.mapView.delegate = self
        self.delegate.delegate = self
        setupNib()
        loadingView.isHidden = false
        takeList()
        let initialLocation = CLLocation(latitude: 45.0630786, longitude: 7.6835545)
        centerMapOnLocation(location: initialLocation)
    }
    
    func takeList()
    {
        manager.takeListAction { (list) in
            self.loadingView.isHidden = true
            if(list.count > 0)
            {
                self.delegate.list = list
                self.list = list
                for l in list
                {
                    let artwork = Artwork(title: l.name!,
                                          locationName: l.address!,
                                          discipline: "",
                                          coordinate: CLLocationCoordinate2D(latitude: Double(l.lat!)!, longitude: Double(l.lng!)!),
                                          poi: l)
                    self.artworks.append(artwork)
                }
                self.filterSwitch.isEnabled = true
                self.mapView.addAnnotations(self.artworks)
                self.tableView.reloadData()
            }
            else
            {
                self.createAlertwith(title: "Errore", message: "Connessione assente")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Punti d'interesse"
        tableView.allowsSelection = true
    }
    //MARK: STARTPAGEDELEGATE
    func goToDetail(poi: POIModel) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DetailTableViewController") as! DetailTableViewController
        controller.poi = poi
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func loadingViewShow(bool: Bool) {
        self.loadingView.isHidden = !bool
    }
    
    func listFiltered(list: [POIModel]) {
        self.delegate.list = list
        self.tableView.reloadData()
        for l in list
        {
            let artwork = Artwork(title: l.name!,
                                  locationName: l.address!,
                                  discipline: "",
                                  coordinate: CLLocationCoordinate2D(latitude: Double(l.lat!)!, longitude: Double(l.lng!)!),
                                  poi: l)
            self.artworksFiltered.append(artwork)
        }
        self.mapView.removeAnnotations(artworks)
        self.mapView.addAnnotations(artworksFiltered)
    }
    //MARK: PRIVATE METHODS
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func setupNib(){
        tableView.register(UINib.init(nibName: "ListTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "List")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
    }
    //MARK: ALERTS
    func createAlertwith(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Riprova", style: .default, handler: actionButton))
        self.present(alert, animated: true, completion: nil)
    }
    func actionButton(action: UIAlertAction)
    {
        takeList()
    }
    //MARK: ACTIONS
    @IBAction func changeKindOfVisual(_ sender: Any) {
        if(visual == KindOfVisual.lista)
        {
            visual = KindOfVisual.mappa
            self.mapView.isHidden = false
            self.tableView.isHidden = true
        }
        else
        {
            visual = KindOfVisual.lista
            self.mapView.isHidden = true
            self.tableView.isHidden = false
        }
    }
    
    @IBAction func filterList(_ sender: Any) {
        if(filterSwitch.isOn)
        {
            delegate.needFilteredList()
        }
        else
        {
            self.delegate.list = list
            self.tableView.reloadData()
            self.mapView.removeAnnotations(artworksFiltered)
            self.mapView.addAnnotations(artworks)
        }
    }
    
    //MARK: MKMAPVIEWDELEGATE
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artwork
        delegate.goToDetail(withPOI: location.poi)
    }
}
